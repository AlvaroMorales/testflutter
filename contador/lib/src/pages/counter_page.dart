import 'package:flutter/material.dart';

class CounterPage extends StatefulWidget {
  @override
  createState() => _ContadorPageState();
}

class _ContadorPageState extends State<CounterPage> {
  final _textStyle = new TextStyle(fontSize: 10);
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mi aplciación flutter"),
        centerTitle: true,
        backgroundColor: Colors.grey,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Número de clickss',
              style: _textStyle,
            ),
            Text('$_count', style: _textStyle),
          ],
        ),
      ),
      floatingActionButton: _crearBotones(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: 30,
        ),
        FloatingActionButton(
            onPressed: _reset, child: Icon(Icons.exposure_zero)),
        Expanded(
          child: SizedBox(),
        ),
        FloatingActionButton(
            onPressed: _reduceCounter, child: Icon(Icons.remove)),
        SizedBox(
          width: 5,
        ),
        FloatingActionButton(onPressed: _addCounter, child: Icon(Icons.add)),
      ],
    );
  }

  void _reduceCounter() {
    setState(() => _count--);
  }

  void _addCounter() {
    setState(() => _count++);
  }

  void _reset() {
    setState(() => _count = 0);
  }
}
